package com.hengboy.security.oauth.jwt;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.cloud.netflix.eureka.server.EnableEurekaServer;

/**
 * @author：于起宇 <p>
 * ================================
 * Created with IDEA.
 * Date：2018/11/2
 * Time：3:19 PM
 * 个人博客：http://blog.yuqiyu.com
 * 简书：http://www.jianshu.com/u/092df3f77bca
 * 码云：https://gitee.com/hengboy
 * GitHub：https://github.com/hengyuboy
 * ================================
 * </p>
 */
@SpringBootApplication
@EnableEurekaServer
public class ServiceCenterApplication {
    /**
     * logger instance
     */
    static Logger logger = LoggerFactory.getLogger(ServiceCenterApplication.class);

    public static void main(String[] args) {
        SpringApplication.run(ServiceCenterApplication.class, args);
        logger.info("「「「「「服务注册中心启动完成」」」」」");
    }
}
