package com.hengboy.security.oauth.jwt;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.cloud.client.discovery.EnableDiscoveryClient;
import org.springframework.cloud.netflix.zuul.EnableZuulProxy;

/**
 * 统一网关zuul 入口类
 *
 * @author：于起宇 <p>
 * ================================
 * Created with IDEA.
 * Date：2018/11/2
 * Time：3:14 PM
 * 个人博客：http://blog.yuqiyu.com
 * 简书：http://www.jianshu.com/u/092df3f77bca
 * 码云：https://gitee.com/hengboy
 * GitHub：https://github.com/hengyuboy
 * ================================
 * </p>
 */
@SpringBootApplication
@EnableZuulProxy
@EnableDiscoveryClient
public class ZuulGateWayApplication {
    /**
     * logger instance
     */
    static Logger logger = LoggerFactory.getLogger(ZuulGateWayApplication.class);

    public static void main(String[] args) {
        SpringApplication.run(ZuulGateWayApplication.class, args);
        logger.info("「「「「「统一网关启动完成」」」」」");
    }
}
