import com.hengboy.security.oauth.jwt.AuthServiceApplication;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.test.context.junit4.SpringRunner;

/**
 * @author：于起宇 <p>
 * ================================
 * Created with IDEA.
 * Date：2018/11/2
 * Time：4:10 PM
 * 个人博客：http://blog.yuqiyu.com
 * 简书：http://www.jianshu.com/u/092df3f77bca
 * 码云：https://gitee.com/hengboy
 * GitHub：https://github.com/hengyuboy
 * ================================
 * </p>
 */
@RunWith(SpringRunner.class)
@SpringBootTest(classes = AuthServiceApplication.class)
public class PassWordTester {
    @Autowired
    private PasswordEncoder passwordEncoder;

    @Test
    public void setPasswordEncoder(){
        System.out.println(passwordEncoder.encode("clientSecret"));;
    }
}
